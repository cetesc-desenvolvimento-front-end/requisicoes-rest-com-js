const base_url = "http://localhost:3000"

const tabela = document.getElementById("dados")


async function listarPessoas() {
  const pessoas = await axios.get(`${base_url}/pessoas`)
    .then((response) => response.data)

  for (let i = 0; i < pessoas.length; i++) {
    const pessoa = pessoas[i];

    const nova_linha = document.createElement("tr")

    nova_linha.innerHTML = `
        <tr>
          <td>${pessoa.id}</td>
          <td>${pessoa.nome}</td>
          <td>${pessoa.telefone}</td>
        </tr >
      `
    nova_linha.id = pessoa.id

    tabela.appendChild(nova_linha)
  }

}

listarPessoas()


async function cadastrarPessoa() {
  const new_pessoa = {
    nome: document.getElementById("nome").value,
    telefone: document.getElementById("telefone").value
  }

  await axios.post(`${base_url}/pessoas`, new_pessoa)
    .then(() => {
      alert("Registro cadastrado com sucesso!")

      // limpar campos de texto
      const inputs = document.getElementsByTagName('input')

      for (let i = 0; i < inputs.length; i++) {
        const element = inputs[i];

        element.value = null
      }

      // remover dados da tabela
      while (tabela.rows.length > 0) {
        tabela.deleteRow(0)
      }

      // atualizar dados tabela
      listarPessoas()
    })
}

async function atualizarPessoa() {
  const pessoa = {
    id: document.getElementById("id").value,
    nome: document.getElementById("nome-edit").value,
    telefone: document.getElementById("telefone-edit").value
  }

  await axios.put(`${base_url}/pessoas/${pessoa.id}`, pessoa)
    .then(() => {
      alert("Registro atualizado com sucesso!")

      // limpar campos de texto
      const inputs = document.getElementsByTagName('input')

      for (let i = 0; i < inputs.length; i++) {
        const element = inputs[i];

        element.value = null
      }

      // remover dados da tabela
      while (tabela.rows.length > 0) {
        tabela.deleteRow(0)
      }

      // atualizar dados tabela
      listarPessoas()
    })
}

async function removerPessoa() {
  const id = document.getElementById("id-remove").value

  await axios.delete(`${base_url}/pessoas/${id}`)
    .then(() => {
      alert("Registro removido com sucesso!")

      // limpar campos de texto
      const inputs = document.getElementsByTagName('input')

      for (let i = 0; i < inputs.length; i++) {
        const element = inputs[i];

        element.value = null
      }

      // remover dados da tabela
      while (tabela.rows.length > 0) {
        tabela.deleteRow(0)
      }

      // atualizar dados tabela
      listarPessoas()
    })
}